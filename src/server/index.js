// import modules
const fs = require("fs")
const parse = require("csv-parse")

// seperate function modules
const noOfMatchesPerYear = require('./noOfMatchesPerYear')
const noOfMatchesPerYearPerTeam = require('./noOfMatchesPerYearPerTeam')
const extraRunsConceded2016 = require('./extraRunsConceded2016')
const economicalBowlers = require('./economicalBowlers')

// file paths variables created
csvFilePathDeliveries = "/../data/deliveries.csv"
csvFilePathMatches = "/../data/matches.csv"

const parser = parse({columns:true},function (err, Matches) {
const parser2 = parse({columns:true},function (err, deliveries) {
    
// problem 1 : Number of matches played per year for all the years in IPL.

const problem1 = noOfMatchesPerYear(Matches)
console.log(problem1)


// problem 2
//Number of matches won per team per year in IPL

const problem2 = noOfMatchesPerYearPerTeam(Matches)
console.log(problem2)


// problem 3 Extra runs conceded per team in the year 2016

const problem3 = extraRunsConceded2016(Matches,deliveries)
console.log(problem3)


// Problem 4 : Top 10 economical bowlers in the year 2015
const problem4 =  economicalBowlers(Matches,deliveries)
console.log(problem4)



})
fs.createReadStream(__dirname+csvFilePathDeliveries).pipe(parser2)
})
fs.createReadStream(__dirname+csvFilePathMatches).pipe(parser)
