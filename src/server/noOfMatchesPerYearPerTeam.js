const fs = require("fs")
function noOfMatchesPerYearPerTeam(Matches)
{
if (Matches === undefined || Matches.length === 0)
    {
        return "CSV file error"
    }
const noOfMatchesPerYearPerTeam = {}
const matchWon = (year) => 
{
    teamWon = {}
    Matches.forEach(match => {
        if (match.season === year)
        {
            if (match.winner in teamWon)
            {
            teamWon[match.winner] += 1
            }
            else
            {
            teamWon[match.winner] = 1
            }
        }
      })
    return teamWon
}

Matches.forEach(match => {
    if ((match.season in noOfMatchesPerYearPerTeam) === false)
    {
        noOfMatchesPerYearPerTeam[match.season] = matchWon(match.season)
    }
})

fs.writeFile('../public/output/noOfMatchesPerYearPerTeam.JSON', JSON.stringify(noOfMatchesPerYearPerTeam), function (err) {
    if (err) throw err;
    console.log('Problem 2 file has been written');}


)
return "File has been written"
}


module.exports = noOfMatchesPerYearPerTeam