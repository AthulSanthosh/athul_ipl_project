const fs = require('fs')
function economicalBowlers(Matches,deliveries)
{
if (Matches === undefined || deliveries === undefined){
    return "Error in CSV file"
}
// first get all the id of matches played in 2015
const id_2015 = Matches.filter(match => match.season === '2015').map(match => match.id)

// function to calculate total runs conceded by each bowler
function totalRunsBowler()
{
totalRuns = {}
deliveries.forEach( delivery => 
    {
        if (delivery.match_id in id_2015){
            if (delivery.bowler in totalRuns){
                totalRuns[delivery.bowler] += parseInt(delivery.total_runs)
            }
            else{
                totalRuns[delivery.bowler] = parseInt(delivery.total_runs)
            }
        }
})
return totalRuns
}


// totalOvers calculation
function totalOvers()
{
// need to calculate maximum overs for each bowlers in each match 
// each match_id => each bowler => maximum over 
totalOvers = {}
deliveries.forEach(delivery => {
    if (delivery.match_id in id_2015)
    {
        eachMatchBowler = {}
        if (delivery.bowler in eachMatchBowler)
        {
            eachMatchBowler[delivery.bowler] = Math.max(parseInt(eachMatchBowler[delivery.bowler]),parseInt(delivery.over))  
        }
        else{
            eachMatchBowler[delivery.bowler] = parseInt(delivery.over) 
            totalOvers[delivery.bowler] = eachMatchBowler[delivery.bowler]
        }
   }
})
return totalOvers
}

testTotalOvers = totalOvers()
testTotalRuns = totalRunsBowler()


function economicalBowlersCalc(testTotalOvers,testTotalRuns)
{
    economicalBowlersList = []
    for (let bowler in testTotalOvers)
    {
        economicalBowlersList.push( [bowler,testTotalRuns[bowler]/testTotalOvers[bowler]])
    }
    
    economicalBowlersList.sort((a,b) => b[1] - a[1])
    top10bowlers = economicalBowlersList.slice(0,10)

    // top 10 bowlers 
    top10bowlers = top10bowlers.map(element => element[0])
    fs.writeFile('../public/output/economicalBowlers.JSON', JSON.stringify(top10bowlers), function (err) {
        if (err) throw err
        console.log('Problem 4 file has been written')
})
}
economicalBowlersCalc(testTotalOvers,testTotalRuns)
return "File has been written"
}

module.exports = economicalBowlers