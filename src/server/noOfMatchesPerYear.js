const fs = require("fs")
function noOfMatchesPerYear(Matches)
{
// error handling 
if (Matches === undefined || Matches.length === 0){
    return "CSV file error"
}
noOfMatches = {}
Matches.forEach(match => {
    if (match.season in noOfMatches){
        noOfMatches[match.season] += 1
    }
    else {
        noOfMatches[match.season] = 1
    }
})
// output the result to the output file
fs.writeFile('../public/output/matchesPerYear.JSON', JSON.stringify(noOfMatches), function (err) {
    if (err) throw err;
    console.log('Problem 1 file has been written');
})
return "File has been written"
}

module.exports = noOfMatchesPerYear;