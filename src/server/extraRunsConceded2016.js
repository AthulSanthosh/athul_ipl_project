// problem 3 Extra runs conceded per team in the year 2016
const fs = require('fs')

function extraRuns(Matches,deliveries) {
// error handling 
if (Matches === undefined || deliveries === undefined){
    return "CSV file error"
}

const id_2016 = Matches.filter(match => match.season === '2016').map(match => match.id)
extraRunsConceded = {}
deliveries.forEach(delivery =>  
{
    if ((delivery.match_id in id_2016) === true)
    {
        if (delivery.bowling_team in extraRunsConceded)
        {   
            extraRunsConceded[delivery.bowling_team] += parseInt(delivery.extra_runs)
        }
        else 
        {
            extraRunsConceded[delivery.bowling_team] = parseInt(delivery.extra_runs)
        }    
    }
}
)
fs.writeFile('../public/output/extraRunsConceded.JSON', JSON.stringify(extraRunsConceded), function (err) {
    if (err) throw err
    console.log('Problem 3 file has been written')
})

return "File has been written"
}

module.exports = extraRuns